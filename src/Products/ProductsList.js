import React, { Component } from "react";
import ProductsContainer from "./ProductsContainer";
import Product from "./Product";
import { css } from "emotion";

export default class ProductsList extends Component {
  render() {
    return (
      <div
        className={css`
          display: grid;
          grid-template-columns: repeat( auto-fit, minmax(250px, 1fr) );
          margin:auto;
          max-width:1600px;
        `}
      >
        {/* This line is not allowed to be modified or changed */}
        <ProductsContainer
          render={products => products.map(product => <Product {...product} />)}
        />
        {/* you are allowed to modify as you see fit below this point */}
      </div>
    );
  }
}
