import React, { Component } from "react";

export default class ProductsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: null,
      error: null
    };
  }

  componentDidMount() {
    // fetch data
    fetch("../../data.json")
      .then(res => res.json())
      .then(
        products => {
          this.setState({
            products: products.items
          });
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    console.log("productContainerStateLog:", this.state.products);
    if (this.state.products !== null) {
      return this.props.render(this.state.products);
    } else return null;
  }
}
