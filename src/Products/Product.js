import React, { Component } from "react";
import { css } from "emotion";

export default class Product extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showMoreInfo: false,
      currentVariant: null
    };
  }

  //function to show more information about a single item
  showMore() {
    const currentState = this.state.showMoreInfo;
    this.setState({ openedNav: !currentState });

    if (this.state.showMoreInfo) {
      this.setState({ showMoreInfo: false });
      document.getElementsByTagName("body")[0].style.overflow = "auto"
    } 
    else this.setState({ showMoreInfo: true, currentVariant:0}),
      document.getElementsByTagName("body")[0].style.overflow = "hidden";
  }

  //function to change a current item variant
  changeCurrentVariant (input) {
    this.setState({
      currentVariant: input
    })
  }

  render() {
    //Start of showMoreInfo section
    if(this.state.showMoreInfo) {
      return (
        <div className = {`showMoreWrapper 
          ${css`
            position: fixed;
            top: 0; left: 0; bottom: 0; right: 0;
            z-index: 1;
            background:rgba(255,255,255,0.9);
            @media (max-width: 600px) {
              display:block;
            }
          `}`
        }>

          <div className = {`showMoreMain 
            ${css`
              background-color:white;
              justify-content: center;
              border: 1px solid black;
              position:fixed;
              max-width:1600px;
              top: 10%; left: 10%; bottom: 10%; right: 10%;
              overflow-y: scroll;
              overflow-x: hidden;
              box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
              @media (max-width: 600px) {
                width:auto;
              }
            `}`
          }>

            <div className = {`showMoreCloseButton 
              ${css`
                position:fixed;
                border-radius: 50%;
                font-weight:700;
                font-size:1.2rem;
                padding:1.4rem;
                width:1rem;
                border:1px solid black;
                text-align:center;
                left:7%;
                top:7%;
                cursor:pointer;
                z-index:1;
                background-color:white;
                  &:hover {
                    background:#1d2951;
                    color:white;
                  }
              `}`
            } 

            onClick = {()=> this.showMore()}   
            > x </div>
            <div className = {`singleProductName 
              ${css`
                position:relative;
                padding:0.4rem;
                height:2rem;
                width:100%;
                font-size:2rem;
                text-align:center;
                color:white;
                background-color:black;
                opacity:0.8;
              `}`
            }>
              {this.props.variantInfos[this.state.currentVariant].name}
            </div>

            <div className = {`singleProductImageWrapper 
              ${css`
                position:relative;
                display:inline-block;
                width:45%;
                height:60%;
                margin-left:2%;
                margin-right:2%;
                padding-top:5rem;
                border-right:1px dotted grey;
                text-align:center;
                @media (max-width: 600px) {
                  display:block;
                  width:100%;
                }
              `}`
            }>

              <div>Pris : {this.props.variantInfos[this.state.currentVariant]["price"]["salesPriceInclVat"]} sek</div>
              <img src={this.props.variantInfos[this.state.currentVariant].imageUrl} 
                className={ css`
                  width:100%;
                  max-width:400px;
                  @media (max-width: 600px) {
                    display:block;
                    width:100%;
                  }
                `}>
              </img>
            </div>

            <p dangerouslySetInnerHTML={{__html:this.props.description}}
              className = {`singleProductDescription 
                ${css`
                  display: inline-block;
                  width:45%;
                  height:80%;
                  margin-left:1%;
                  margin-right:1%;
                  overflow-x:hidden;
                  overflow-y:auto;
                  @media (max-width: 600px) {
                    margin:0.5rem;
                    width:100%;
                    height:auto;
                  }
                `}`
              }>
            </p>

            <div className={`alternatives 
              ${css`
                position:relative;
                width:auto;
                margin-left:2%;
                margin-right:2%;
                  @media (max-width: 600px) {
                    width:100%;
                  }
             `}`
            }>

            <h3>Fler varianter: </h3>
              {
              Array.from(this.props.variantInfos).map((item,i)=> {
                if(this.state.currentVariant !== i) {
                  return (
                    <div key={item.productId}
                      className={`alternativeItem 
                      ${css`
                        display:inline-block;
                        width:11rem;
                        padding-right:3rem;
                        text-align:center;
                          @media (max-width: 600px) {
                           width:100%;
                        }
                      `}`
                    } 

                    onClick = {()=> this.changeCurrentVariant(i)}>
                      {item.name}
                      <img className={css`
                        width:auto;
                        height:170px;
                      `}

                      src={item.imageUrl}
                      />
                  </div>
                )}
              })
            }
            </div>
          </div>
        </div>
      )} //End of showMoreInfo section

    //Start of all products view
    return (
      <div className={css`
            display: flex;
            width: 100%;   
      `}>

      <div onClick = {()=> this.showMore(0)}
        className={css`
          margin: 1rem; 
          border:  1px solid gray;
          text-align:center;
          font-family: 'Open Sans Condensed', sans-serif;
          cursor: pointer;
      `}>

      <h5 className={css`
           background-color:black;
           color:white;
      `}>

      {this.props.variantInfos[0]["name"]}</h5>

      <img className={css`
            height: 150px;
      `}

          src={this.props.variantInfos[0]["imageUrl"]}
      />
          <div className={`productDivBottom 
            ${css`
              background-color: black;
              color: white;
              opacity:0.8;
              padding:0.4rem;
              height:8rem;
            `}`
          }>
            <p>Pris : {this.props.variantInfos[0]["price"]["salesPriceInclVat"]} sek</p>
            <p dangerouslySetInnerHTML={{__html:this.props.description.substring(0,80) + "..."}}></p>
          </div>
        </div>
        <h5>{name}</h5> 
      </div> 
    )//End of all products view
  }
}

